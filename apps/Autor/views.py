from django.shortcuts import render, redirect, get_object_or_404

from apps.Autor.models import Autor
from apps.Autor.forms import AutorForm
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.

def index(request):
    autores_list = Autor.objects.all()
    page = request.GET.get('page', 1)
    
    paginator = Paginator(autores_list, 1)
    try:
        autores = paginator.page(page)
    except PageNotAnInteger:
        autores = paginator.page(1)
    except EmptyPage:
        autores = paginator.page(paginator.num_pages)
    
    return render(request, 'Autor/index.html', {'autores': autores, 'messages': messages.get_messages(request)})

def create(request):
    form = AutorForm(request.POST or None)
    if form.is_valid():
        form.save()
        messages.add_message(request, messages.SUCCESS, 'Autor Creado')
        return redirect('autor:index')
    return render(request, 'autor/form.html', {'form': form})

def update(request, autorid):
    autor = get_object_or_404(Autor, id=autorid)
    form = AutorForm(request.POST or None, instance=autor)
    if form.is_valid():
        form.save()
        messages.add_message(request, messages.SUCCESS, 'Autor Actualizado')
        return redirect('autor:index')
    return render(request, 'autor/form.html', {'form': form})

def delete(request, autorid):
    autor = get_object_or_404(Autor, id=autorid)
    autor.delete()
    messages.add_message(request, messages.SUCCESS, 'Autor Eliminado')
    return redirect('autor:index')
