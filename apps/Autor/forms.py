from django import forms

from apps.Autor.models import Autor

class AutorForm(forms.ModelForm):
    class Meta:
        model = Autor
        fields = ['apellido', 'nombre', 'usuario']
        widgets = {
            'apellido': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'usuario': forms.TextInput(attrs={'class': 'form-control'}),
        }