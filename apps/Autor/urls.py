from django.conf.urls import url

from apps.Autor import views

## r'^ruta$', r'^ruta/$'
## (?P<nombre>\w+) string, (?P<nombre>\d+) integer

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^nuevo$', views.create, name='crear'),
    url(r'^(?P<autorid>\d+)/actualizar/$', views.update, name='actualizar'),
    url(r'^(?P<autorid>\d+)/eliminar/$', views.delete, name='eliminar'),
]
