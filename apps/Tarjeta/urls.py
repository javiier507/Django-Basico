from django.conf.urls import url

from apps.Tarjeta import views

## r'^ruta$', r'^ruta/$'
## (?P<nombre>\w+) string, (?P<nombre>\d+) integer

urlpatterns = [
    url(r'^$', views.TarjetaList.as_view(), name='index'),
    url(r'^nuevo$', views.TarjetaCreate.as_view(), name='crear'),
]
