from django.views.generic import ListView, CreateView
from django.core.urlresolvers import reverse_lazy

from apps.Tarjeta.models import Tarjeta
from apps.Tarjeta.forms import TarjetaForm

# Create your views here.

class TarjetaCreate(CreateView):
    model = Tarjeta
    form_class = TarjetaForm
    template_name = 'tarjeta/form.html'
    success_url = reverse_lazy('tarjeta:index')

class TarjetaList(ListView):
    model = Tarjeta
    template_name = 'tarjeta/index.html'
