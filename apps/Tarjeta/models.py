from django.db import models

from apps.Autor.models import Autor

# Create your models here.

class Tarjeta(models.Model):
    titulo = models.CharField(max_length=150)
    contenido = models.TextField(max_length=300)
    imagen = models.ImageField(upload_to='media/')
    autor = models.ForeignKey(Autor, blank=True, null=True, on_delete=models.CASCADE)
